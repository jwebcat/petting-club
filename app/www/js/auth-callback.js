angular.module('ionicloop.auth', ['lbServices', 'ionic'])

.controller('AuthCallbackCtrl', function ($scope, $state, $location, $window, LoopBackAuth) {

    // Check for 'auth' UI router state
    if ($state.is('auth')) {
      // Get params from 'auth' state from FB redirect
      var params = $location.search();

      // check for params we need to associate logged in user with LoopBackAuth
      if (params.accessToken && params.userId) {
        /**
         * Handle auth response by adding properties to the LoopBackAuth
         * and then calling save. (Same effect as login)
         */
        LoopBackAuth.setUser(params.accessToken, params.userId);
        // Saves the values to local storage.
        LoopBackAuth.save();

        console.log("login success");
        $state.go('tabs.account')
      }
      else {
        /**
         * Something isn't right.
         * Redirect the user to the login page.
         */
        console.log("login fail.");
        $location.path('login');
      }
    }
  });
