// Register our app and require our necessary services etc.
angular.module('ionicloop', ['ionicloop.account','ionicloop.login', 'ionicloop.register', 'ionicloop.auth', 'ionicloop.services', 'ionic', 'lbServices'])

    .run(function ($ionicPlatform, MyUser) {
        $ionicPlatform.ready(function () {
          /**
           * Hide the accessory bar by default (remove this to show the accessory bar
           * above the keyboard for form inputs)
           */
          if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
          }
          if (window.StatusBar) {
            StatusBar.styleDefault();
          }
        });
        // Check if MyUser is authenticated
        if (MyUser.getCachedCurrent() == null) {
            MyUser.getCurrent();
        };
    })
    // Config for LoopBack Angular SDK
    .config(function(LoopBackResourceProvider) {
      // Use a custom auth header instead of the default 'Authorization'
      LoopBackResourceProvider.setAuthHeader('X-Access-Token');
      // Change the URL where to access the LoopBack REST API server
      LoopBackResourceProvider.setUrlBase('http://localhost:3000/api');
    })
    .config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
        $stateProvider
            /**
             * Auth state to capture accessToken and userId from
             * FB server auth after FB login redirect
             */
            .state('auth', {
                url: '/auth/callback?accessToken&userId',
                templateUrl: 'templates/login.html',
                controller: 'AuthCallbackCtrl'
            })
            .state('login', {
                url: '/login',
                templateUrl: 'templates/login.html',
                controller: 'LoginCtrl'
            })
            .state('register', {
                url: '/register',
                templateUrl: 'templates/register.html',
                controller: 'RegisterCtrl'
            })
            .state('tabs', {
                url: '/tab',
                abstract: true,
                templateUrl: 'templates/tabs.html'
            })
            .state('tabs.account', {
                url: '/account',
                views: {
                    'account-tab': {
                        templateUrl: 'templates/account.html',
                        controller: 'AccountCtrl'
                    }
                }
            });

        $urlRouterProvider.otherwise('/login');
    })
;
