angular.module('ionicloop.login', ['lbServices', 'ionic'])
    .controller('LoginCtrl', function ($scope, MyUser, $location, $ionicPopup, $facebookLogin) {

        // Redirect user to the app if already logged in
        if (MyUser.getCachedCurrent()!==null) {
           $location.path('tab/account');
        }
        /**
         * Currently you need to initialiate the variables
         * you use whith ng-model. This seems to be a bug with
         * ionic creating a child scope for the ion-content directive
         */
        $scope.credentials = {};

        // attach $facebookLogin factory to scope
        $scope.fbLogin = $facebookLogin;

        /**
         * @name showAlert()
         * @param {string} title
         * @param  {string} errorMsg
         * @desctiption
         * Show a popup with the given parameters
         */
        $scope.showAlert = function (title, errorMsg) {
            var alertPopup = $ionicPopup.alert({
                title: title,
                template: errorMsg
            });
            alertPopup.then(function (res) {
                console.log($scope.loginError);
            });
        };

        /**
         * @name login()
         * @description
         * sign-in function for users which created an account
         */
        $scope.login = function () {
            $scope.loginResult = MyUser.login({include: 'user', rememberMe: true}, $scope.credentials,
                function () {
                    var next = $location.nextAfterLogin || 'tab/account';
                    $location.nextAfterLogin = null;
                    $location.path(next);
                },
                function (err) {
                    $scope.loginError = err;
                    $scope.showAlert(err.statusText, err.data.error.message);
                }
            );
        };
        /**
         * @name goToRegister()
         * @description
         * function to navigate to the register account view
         */
        $scope.goToRegister = function () {
            $location.path('register');
        };

    });
