angular.module('ionicloop.register', ['lbServices', 'ionic'])
    .controller('RegisterCtrl', function ($scope, MyUser, $ionicPopup, $location, $facebookLogin) {
        /**
         * Currently you need to initialiate the variables
         * if you want to use them in the controller. This seems to be a bug with
         * ionic creating a child scope for the ion-content directive
         */
        $scope.registration = {};

        // attach $facebookLogin factory to scope
        $scope.fbLogin = $facebookLogin;

        // Redirect user to the app if already logged in
        if (MyUser.getCachedCurrent() !== null) {
            $location.path('tab/account');
        }

        /**
         * @name register()
         * @description
         * register a new user and login
         */
        $scope.register = function () {
            $scope.registration.created = new Date().toJSON();
            $scope.user = MyUser.create($scope.registration)
                .$promise
                .then(function (res) {
                    // Sign in new user
                    MyUser.login({include: 'user', rememberMe: true}, $scope.registration)
                        .$promise
                        .then(function (res) {
                          console.log(JSON.stringify(res))
                            $location.path('tab/account')
                        }, function (err) {
                            $scope.loginError = err;
                            $scope.showAlert(err.statusText, err.data.error.message);
                        })
                }, function (err) {
                    $scope.registerError = err;
                    $scope.showAlert(err.statusText, err.data.error.message);
                });
        };

        /**
         * @name showAlert()
         * @param {string} title
         * @param  {string} errorMsg
         * @description
         * Show a popup with the given parameters
         */
        $scope.showAlert = function (title, errorMsg) {
            var alertPopup = $ionicPopup.alert({
                title: title,
                template: errorMsg
            });
            alertPopup.then(function () {
                console.log($scope.loginError);
            });
        };
    });
