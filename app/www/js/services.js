angular.module('ionicloop.services', ['lbServices'])

.factory('$facebookLogin', function(MyUser) {
  return function() {
    // URL for the Loopback server passport-facebook login
    var url = 'http://localhost:3000/auth/facebook';
    // bypass UI router and navigate window to FB auth URL
    window.open(url, '_self');
  };
})
