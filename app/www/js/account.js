angular.module('ionicloop.account', ['lbServices', 'ionic'])
    .controller('AccountCtrl', function ($scope, $location, MyUser) {

        // Redirect user to login if not authenticated
        if (!MyUser.isAuthenticated()) {
           $location.path('/login');
        }

        // put current user into scope
        $scope.currentUser = MyUser.getCurrent();
        /**
         * @name logout()
         * logout user and redirect to the login page
         */
        $scope.logout = function () {
            MyUser.logout(function () {
                $location.path('/login');
            });
        }

    });
